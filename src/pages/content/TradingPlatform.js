import React from 'react';

import TradingPlatformContent from '../../components/content/tradingplatform'
import NavigationBLK from "../../components/navigation-blk"
import "../Home.css"



function TradingPlatform () {

    return (
        <div className="container">
            <NavigationBLK />
            <TradingPlatformContent />

         </div>
    )

}
export default TradingPlatform
import Navigation from "../components/navigation"

import Footer from "../components/footer"
import ThanksComp from "../components/auth/thankyou"


function ThankYou () {

    return (
        <>

            <Navigation />
            <ThanksComp />
            <Footer/>

        </>
    )

}
export default ThankYou
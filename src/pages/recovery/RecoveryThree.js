
import RecoverThree from '../../components/auth/recover/RecoverThree'
import Footer from '../../components/footer'
import Navigation from '../../components/navigation'


function RecoveryThree () {

    return (
<>
            <Navigation />
            <RecoverThree />

            <Footer />

</>
    )

}
export default RecoveryThree
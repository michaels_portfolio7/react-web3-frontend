
import RecoverOne from '../../components/auth/recover/RecoverOne'
import Footer from '../../components/footer'
import Navigation from '../../components/navigation'

function RecoveryOne () {

    return (
<>
            <Navigation />
            <RecoverOne />

            <Footer />

</>
    )

}
export default RecoveryOne
import Navigation from '../components/navigation'
import ForgotPass from '../components/auth/forgot'
import Footer from '../components/footer'

function Forgot () {

    return (
<>
            <Navigation />
            <ForgotPass />
            <Footer />

</>
    )

}
export default Forgot
import Navigation from "../components/navigation"
// import Header from "../components/header"
// import Footer from "../components/footer"
import Login from '../components/auth/login'
import Footer from "../components/footer"


function LoginPage () {

    return (
        <>
            <Navigation />
            <Login />
            <Footer/>
</>
    )

}
export default LoginPage
import Home from './pages/Home';
import Register from './pages/Register'
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Login from './components/auth/login';
// import useToken from './components/Token/useToken';
import { PrivateRoute } from './components/Token/PrivateRoute';
import Test from './components/test';
import Forgot from './pages/Forgot';
// import RecoverToken from './components/auth/tokenrecover';
import ContentLearn from './pages/content/ContentLearn';
import TradingPlatform from './pages/content/TradingPlatform';
import WhyCassandra from './pages/content/WhyKassandra'
import Dashboard from './pages/Dashboard';
import Recover from './pages/Recover';
import ThankYou from './pages/ThankYou';
import RecoveryOne from './pages/recovery/RecoveryOne';
import RecoveryTwo from './pages/recovery/RecoveryTwo';
import RecoveryThree from './pages/recovery/RecoveryThree';
import RecoveryFour from './pages/recovery/RecoveryFour';
import Terms from './pages/Terms';
import TradingDashboard from './components/trading/tradingindex';
import WalletCardEthers from './components/web3/connect';
import Buy from './components/web3/buy';
import IsModal from './components/web3/modal';


function App() {


// const { token, setToken } = useToken();

// function isAuth(token) {
//   if(!token) {
//     return <Login setToken={setToken} />
//   }
// }

  return (
    <Router>

      <Switch>

        <Route exact path="/"><Home /></Route>

        <Route exact path="/learningplatform"><ContentLearn /></Route>
        <Route exact path="/tradingplatform"><TradingPlatform /></Route>
        <Route exact path="/whykassandra"><WhyCassandra /></Route>

        
        <PrivateRoute path="/test" component={Test} />

        <Route path="/login">
          {/* <Login setToken={setToken} /> */}
          <Login  />

        </Route>

        <Route path="/register">
          <Register />
        </Route>

        <Route path="/forgot">
          <Forgot />
        </Route>

        <Route path="/tokenrecover">
          <Recover />
        </Route>

        <Route path="/recover-1">
          <RecoveryOne/>
        </Route>

        <Route path="/recover-2">
          <RecoveryTwo/>
        </Route>

        <Route path="/recover-3">
        <RecoveryThree/>

        </Route>

        <Route path="/recover-4">
          <RecoveryFour/>
        </Route>



        <Route path="/thankyou">  
          <ThankYou />
        </Route>

        <Route path="/terms">  
        <Terms/>
        </Route>

        <Route path="/dashboard">
          <Dashboard />
        </Route>

        <Route path="/trading">
          <TradingDashboard/>
        </Route>

        <Route path="/wallet">
          <IsModal/>
          <WalletCardEthers/>
          <Buy/>

        </Route>

        <Route path="/buy">

          <Buy/>
        </Route>


      </Switch>

    </Router>


    
  );
}

export default App;

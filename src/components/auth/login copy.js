import './form.css'
import 'react-phone-number-input/style.css'
import PhoneInput from 'react-phone-number-input'
import React from 'react';
import { useState } from "react";
import PropTypes from 'prop-types';
import { Redirect, Route } from "react-router";
import { withRouter } from 'react-router';
import Navigation from '../navigation'

async function loginUser(credentials) {

    return fetch('http://127.0.0.1:8087/authenticate', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(credentials)
    })
      .then(data => data.json())
   }


export default function Login ({setToken}) {

    const [username, setUserName] = useState();
    const [password, setPassword] = useState();
    const [idtoken, setIDToken] = useState();
    const [submitted, setSubmitted] = useState(false);


    const handleSubmit = async e => {
        e.preventDefault();
        const token = await loginUser({
            username,
            password,
            idtoken
        });
        setToken(token);
        console.log(token);
        for (const [key, value] of Object.entries(token)) {
          console.log(key, value);
        }
                // if (token === '')
        setSubmitted(false);
    }
          if (submitted) {
            return <Redirect push to={{
              pathname: '/dashboard'
            }}
            />
          }   

    console.log(handleSubmit)


    return (
        <>

        {/* <h1 className="form-title">Create Account</h1> */}
        <Navigation />

        <div className="account-form-div">


        <form className="form-create-account" onSubmit={handleSubmit}>
            <label id="title" for="title">Log In</label><br /><br /><br />

            <label for="username">Username</label><br />
            <input type="text" placeholder="Username" name="username" onChange={e => setUserName(e.target.value)}/><br /><br />

           
            <label for="password">Password</label><br />
            <input type="password" placeholder="Password" name="password" onChange={e => setPassword(e.target.value)}/><br /><br />

            <label for="pword">Security Token</label><br />
            <input type="text" placeholder="Token" name="idtoken" onChange={e => setIDToken(e.target.value)}/><br /><br />


            <button type="submit">Login</button><br /><br />

            <span className="login-in-tiny">Already a member? <span id="green">Log In</span></span>


        </form>

        </div>


        </>
    )
}
Login.propTypes = {
    setToken: PropTypes.func.isRequired

  };

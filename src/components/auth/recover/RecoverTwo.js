import { Link } from "react-router-dom";

export default function RecoverTwo() {
    return <div style={{justifyContent: 'center', display: 'flex'}}>
  
  <form className="form-create-account" style={{ width: '500px', paddingTop: "50px"}}>
                  <label id="title" for="title">Recover Security Token</label><br />
                  <h4>Please enter your credentials to recover your security token.</h4>
              
                      <label for="username">Username</label><br />
                      <input type="text" placeholder="Enter Username..." name="username"/><br /><br />

                      <label for="username">Email</label><br />
                      <input type="text" placeholder="Enter Email..." name="username"/><br /><br />
                      <label for="username">Password</label><br />
                      <input type="text" placeholder="Enter Email..." name="username"/><br /><br />
                      <label for="username">Phone</label><br />
                      <input type="text" placeholder="Enter Email..." name="username"/><br /><br />
                      <label for="username">Backup Security Token</label><br />
                      <input type="text" placeholder="Enter Email..." name="username"/><br /><br />

                      <button
                      type="submit"
                      className="btn btn-primary mt-3"
                      value="w3docs"
                      ><Link style={{color: 'white', textDecoration: 'none'}} to='/recover-3'>Submit</Link></button><br /><br />

                    <span className="login-in-tiny"> <span id="green">I do not have all the required information.</span></span>
              </form>
    </div>;
  }
  
import { Link } from "react-router-dom";

export default function RecoverThree() {
    return <div style={{justifyContent: 'center', display: 'flex'}}>

<form className="form-create-account" style={{ width: '500px', paddingTop: "50px"}}>
                  <label id="title" for="title">Unable to Recover Security Token</label><br />
                  <h4>Please enter your Document Identification and your Social Security Number to recover your Security Token. </h4> <br/>
              
                      <label for="username">Document Identification</label><br />
                      <input type="text" placeholder="Enter Username..." name="username"/><br /><br />

                      <label for="username">Social Security Number</label><br />
                      <input type="text" placeholder="Enter Email..." name="username"/><br /><br />

                      <button
                      type="submit"
                      className="btn btn-primary mt-3"><Link style={{color: 'white', textDecoration: 'none'}} to='/recover-4'>Submit</Link></button><br /><br />

                    <span className="login-in-tiny"> <span id="green">I do not have all the required information.</span></span>
              </form>

    </div>;
  }
  
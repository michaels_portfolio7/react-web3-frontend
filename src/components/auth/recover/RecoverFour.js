import { ScheduleMeeting } from 'react-schedule-meeting';
import '../schedule.css'
import { Link } from 'react-router-dom';

export default function RecoverFour() {
  const availableTimeslots = [0, 1, 4, 5, 6, 7, 8].map((id) => {
    return {
      id,
      startTime: new Date(new Date(new Date().setDate(new Date().getDate() + id)).setHours(10, 0, 0, 0)),
      endTime: new Date(new Date(new Date().setDate(new Date().getDate() + id)).setHours(17, 0, 0, 0)),
    };
  });

return(
  <div style={{justifyContent: 'center', display: 'flex'}}>

    
    <div className="scheduler" style={{width: '700px', paddingTop: '50px'}}>
                          <label id="title" for="title">Unable to Recover Security Token</label><br />
              <h4>Please enter your Document Identification and your Social Security Number to recover your Security Token. </h4>
              <div style={{position: 'relative', justifyContent: 'center'}}>
              <label for="username">Available date & Time </label><br />
                  <input style={{width: '300px', position: 'absolute'}} type="text" placeholder="07/18/2021  2:00 AM" name="username"/><br /><br />
              </div>
                             
          <ScheduleMeeting
            borderRadius={10}
            primaryColor="#3f5b85"
            eventDurationInMinutes={30}
            availableTimeslots={availableTimeslots}
            onStartTimeSelect={console.log}
            
        />

<div style={{width: '100%', display: 'flex', justifyContent: 'center'}}>
<button
                  type="submit"
                  style={{width: '300px', position: 'relative'}}
                  className="btn btn-primary mt-3"><Link style={{color: 'white', textDecoration: 'none'}} to='/thankyou'>Submit</Link></button><br /><br />
</div>


    </div>

 </div>

)
  }
  

  
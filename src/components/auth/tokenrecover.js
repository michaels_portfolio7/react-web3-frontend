import './form.css'
import 'react-phone-number-input/style.css'
import React, {useState} from 'react';
import Scheduler from '../auth/schedule';

export default function RecoverToken() {

  const page = useState(2);

    // function goToPage(){
    //   if (page === 4) return;
    //   setPage((page) => page+1);
    // }

  return (
    <div className="account-form-div">
      {page === 1 && <RecoveryOne /> }
      {page === 2 && <RecoveryTwo /> }
      {page === 3 && <RecoveryThree /> }
      {page === 4 && <RecoveryFour /> }






    </div>
  )
}

function RecoveryOne() {
  return <div>Page1

          <form className="form-create-account">
                  <label id="title" for="title">Security Token Help</label><br />
                  <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
                  Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin 

                  The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompan </p><br /><br />
                    
                      <label for="username">Email</label><br />
                      <input type="text" placeholder="Enter Email..." name="username"/><br /><br />

                      <button
                      type="submit"
                      className="btn btn-primary mt-3">Recover Security Token</button><br /><br />

                    <span className="login-in-tiny">Not a current Kassandra member? <span id="green">Create Account</span></span>
              </form>
  </div>;
}

function RecoveryTwo() {
  return <div>

          <form className="form-create-account">
                  <label id="title" for="title">Recover Security Token</label><br />
                  <h4>Please enter your credentials to recover your security token.</h4>
              
                      <label for="username">Username</label><br />
                      <input type="text" placeholder="Enter Username..." name="username"/><br /><br />

                      <label for="username">Email</label><br />
                      <input type="text" placeholder="Enter Email..." name="username"/><br /><br />
                      <label for="username">Password</label><br />
                      <input type="text" placeholder="Enter Email..." name="username"/><br /><br />
                      <label for="username">Phone</label><br />
                      <input type="text" placeholder="Enter Email..." name="username"/><br /><br />
                      <label for="username">Backup Security Token</label><br />
                      <input type="text" placeholder="Enter Email..." name="username"/><br /><br />

                      <button
                      type="submit"
                      className="btn btn-primary mt-3"
                      onclick={RecoverToken.goToPage}
                      value="w3docs"
                      >Recover Security Token</button><br /><br />

                    <span className="login-in-tiny"> <span id="green">I do not have all the required information.</span></span>
              </form>


  </div>;
}

function RecoveryThree() {
  return <div>

          <form className="form-create-account">
                  <label id="title" for="title">Unable to Recover Security Token</label><br />
                  <h4>Please enter your Document Identification and your Social Security Number to recover your Security Token. </h4> <br/>
              
                      <label for="username">Document Identification</label><br />
                      <input type="text" placeholder="Enter Username..." name="username"/><br /><br />

                      <label for="username">Social Security Number</label><br />
                      <input type="text" placeholder="Enter Email..." name="username"/><br /><br />

                      <button
                      type="submit"
                      className="btn btn-primary mt-3">Submit</button><br /><br />

                    <span className="login-in-tiny"> <span id="green">I do not have all the required information.</span></span>
              </form>


  </div>;
}

function RecoveryFour() {
  return <div>
          <Scheduler/>
        </div>;
}

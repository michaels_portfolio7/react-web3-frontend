import React from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from "yup";
import './form.css'
import { useState } from "react";
import axios from "axios";
import { Link } from 'react-router-dom';

import styled from 'styled-components';

const HoverText = styled.p`
margin: 0;
padding: 0;
display: inline-block;
	:hover {
        color: black;
		cursor: pointer;
	}
`

const schema = yup.object({
  username: yup.string().required(),
  email: yup.string().email().required(),
  password: yup.string().min(6).required("Password Required"),

  passconfirm: yup.string().min(6)
    .when("password", {
      is: (val: any) => (val && val.length > 0 ? true : false),
      then: yup.string().oneOf(
        [yup.ref("password")],
        "Both password need to be the same"
      ),
    })
    .required("Confirm Password Required"),

  phone: yup.number().required("Enter Valid"),
  checkbox: yup.boolean().oneOf([true], 'Must Accept Terms and Conditions'),

}).required();

export default function RegisterForm() {
  const [username, setUsername] = useState()
  const [email, setEmail] = useState()
  const [password, setPassword] = useState()
  const [phone, setPhone] = useState()
  const [data, setData] = useState(null);
  const [loading, setLoading] = useState(false);
  const [isError, setIsError] = useState(false);
  const [test, setTest] = useState(false)

  const { register, handleSubmit, formState:{ errors } } = useForm({
    resolver: yupResolver(schema)
  });

  const onSubmit = () => {
    setLoading(true);
    setIsError(false);
    setTest(false);

    const data = {
      username: username,
      email: email,
      password: password,
      phone: phone
    }
    axios.post('http://127.0.0.1:8087/user', data)
    // .then(function (data) {
    //   console.log(data.data);
    //   console.log(data.status);
    //   console.log(data.statusText);
    //   console.log(data.headers);
    //   console.log(data.config);
    // })
    .then(res => {
      setData(res.data);
      setUsername('');
      setEmail('');
      setPassword('');
      // setPassconfirm('');
      setPhone('');
      setLoading(false);
      if (res.status === 200) {
        window.location = "/thankyou"
      }

    })
    .catch(function (error) {
        setTest(error.response.data);

        // console.log(error);
        // console.log(error.response.status);
  
        // console.log(error.response.data);
  
        setLoading(false);
        setIsError(true);
        console.log(setIsError)
        console.log(setTest)

    });
      
  
  //   .then(function(next) {
  //     window.location = "/thankyou"
  // });
  }



  return (
    <div className="account-form-div">

    <form className="form-create-account" onSubmit={handleSubmit(onSubmit)} >
    <label id="title" for="title">Create Account</label><br /><br /><br />

      <label for="username">Username</label><br />
      <input {...register("username")} value={data?.username}  onChange={e => setUsername(e.target.value)} placeholder="Username"  />
      <p style={{fontSize: '9px', color: 'red'}}>{errors.username?.message}</p><br />

      <label for="username">Email</label><br />
      <input {...register("email")} value={data?.email} onChange={e => setEmail(e.target.value)} placeholder="Email"/>
      <p style={{fontSize: '9px', color: 'red'}}>{errors.email?.message}</p><br />

      <label for="username">Password</label><br />
      <input {...register("password")} value={data?.password} onChange={e => setPassword(e.target.value)} type="password" placeholder="Password"/>
      <p style={{fontSize: '9px', color: 'red'}}>{errors.password?.message}</p><br />

      <label for="username">Password Confirmation</label><br />
      <input {...register("passconfirm")} value={data?.passconfirm} type="password" placeholder="Password Confirmation"/>
      <p style={{fontSize: '9px', color: 'red'}}>{errors.passconfirm?.message}</p><br />

      <label for="username">Phone</label><br />
      <input {...register("phone")} value={data?.phone} onChange={e => setPhone(e.target.value)} placeholder="Enter phone number"/>
      <p style={{fontSize: '9px', color: 'red'}}>{errors.phone?.message}</p><br />
      
      <input {...register("checkbox")} id="checkbox"  type="checkbox" placeholder="f" name="checkbox" />
      <span>I agree to the <Link to="/terms"> Terms and Conditions</Link></span><br />
      <p style={{fontSize: '9px', color: 'red', paddingLeft: '27px'}}>{errors.checkbox?.message}</p><br />

      {/* <button
             type="submit"
             className="btn btn-primary mt-3"
            //  onClick={handleSubmit}
            >Create Account</button> */}

{isError && <small className="mt-3 d-inline-block text-danger">  Something went wrong. Please try again later.</small>}
{test && <small className="mt-3 d-inline-block text-danger"> {JSON.stringify(test)}</small>}

        <button
          type="submit"
          className="btn btn-primary mt-3"
          onSubmit={handleSubmit(onSubmit)}
          disabled={loading}
        >{loading ? 'Loading...' : 'Submit'}</button>
        {data && <div className="mt-3">
          <strong>Output:</strong><br />
          <pre>{JSON.stringify(data, null, 2)}</pre>
        </div>
        }

        <div className="form-footer" style={{textAlign: 'center'}}><br/>
        <span className="login-in-tiny">Already a member? <Link to="/login" style={{cursor: 'pointer',color: 'rgba(63, 227, 200, 1)', fontWeight: 'bold'}}><HoverText>Log In</HoverText></Link></span>
        </div>
    </form>

    </div>
  );
}
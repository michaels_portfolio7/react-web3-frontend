import './form.css'
import 'react-phone-number-input/style.css'
import React from 'react';
import { useState } from "react";
import axios from "axios";
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { AiOutlineQuestionCircle } from 'react-icons/ai';

const HoverText = styled.p`
margin: 0;
padding: 0;
display: inline-block;
	:hover {
        color: black;
		cursor: pointer;
	}
`

export default function ForgotPass() {

  const [username, setUsername] = useState()
  // const [idtoken, setIDToken] = useState()
  // const [data, setData] = useState(null);
  // const [data, setData] = useState()


  const handleSubmit = () => {
    const data = {
      username: username,
      
    }
    const headers = { 
      'content-type': 'application/json'
  }

  axios.put('http://45.77.112.99:8000/forgotpassword', data, { headers }).then(res => {
    // setData(res.data);
    setUsername('');


  })

  }
  return (
    <div className="account-form-div">


    <form className="form-create-account">
        <label id="title" for="title">Forgot Account Credentials</label><br />
<p>Please enter your Username or Password and your Security Token to recover your account credentials. </p><br /><br />
        <label for="username">Username</label><br />
        <input
         type="text" 
         placeholder="Username" 
         name="username" 
         value={username}
         onChange={e => setUsername(e.target.value)} /><br /><br />


        {/* <label for="pword">Password</label><br />
        <input 
        type="password" 
        placeholder="Password" 
        name="password" 
        value={password}
        onChange={e => setPassword(e.target.value)} /><br /><br /> */}

        <label for="username">Security TokenSecurity Token<Link to="/recover-1"><AiOutlineQuestionCircle/></Link></label><br />
        <input
         type="text" 
         placeholder="Security Token" 
         name="idtoken" 
         value=""
          /><br /><br />


        <button
         type="submit"
         className="btn btn-primary mt-3"
         onClick={handleSubmit}>Submit</button><br /><br />

<div className="form-footer" style={{textAlign: 'center'}}>

        <span className="login-in-tiny">Not a current Kassandra member? <Link to="/register" style={{cursor: 'pointer',color: 'rgba(63, 227, 200, 1)', fontWeight: 'bold'}}><HoverText>Create Account</HoverText></Link></span>
        </div>


    </form>

    </div>
  )

}
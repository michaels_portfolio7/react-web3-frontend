import './form.css'
import 'react-phone-number-input/style.css'
// import PhoneInput from 'react-phone-number-input'
import React from 'react';
import { useState } from "react";
import PropTypes from 'prop-types';
import { Redirect } from "react-router";
import Navigation from '../navigation'
import { Link } from 'react-router-dom';
import styled from 'styled-components';

import { AiOutlineQuestionCircle } from 'react-icons/ai';

const HoverText = styled.p`
margin: 0;
padding: 0;
display: inline-block;
	:hover {
        color: black;
		cursor: pointer;
	}
`

async function loginUser(credentials) {

       return fetch('http://45.77.112.99:8000/authenticate', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(credentials)
    })
    .then((response) => {
      if(response.ok) {
         return response.json();
          // console.log(response.json())

      } else {
          throw new Error('Server response wasn\'t OK');
      }
    })
    // .then((json) => {
    //   return json.access_token;
    //   console.log(json.access_token)
    // });



    // .then(
    //   console.log('awesome')
    // )
    // .then((response) => {
    //   return response.access_token;
    //   console.log(response.access_token)
    // })

    // .then((json) => {
    //   return json.;
    // });
    //   .then(data => data.json())

   }


export default function Login ({setToken}) {

    const [username, setUserName] = useState();
    const [password, setPassword] = useState();
    const [idtoken, setIDToken] = useState();
    const [submitted, setSubmitted] = useState(false);


    const handleSubmit = async e => {
        e.preventDefault();
        const token = await loginUser({
            username,
            password,
            idtoken
        });
        console.log(token)
        console.log(token.access_token)
        setToken(token.access_token);
        // console.log(token);
        // console.log(token.access_token);


  
        // if (Object.keys(token).stringify !== 'access_token') {
        //   console.log('yes')
        //   // setSubmitted(true);
        // }
        // else {
        //   console.log('no')
        //   // setSubmitted(false);
        // }

  //  ### ORIGINAL
        setSubmitted(true);
  //  ### ORIGINAL


    }
          if (submitted) {
            return <Redirect push to={{
              pathname: '/dashboard'
            }}
            />
          }   

    console.log(handleSubmit)


    return (
        <>

        {/* <h1 className="form-title">Create Account</h1> */}
        <Navigation />

        <div className="account-form-div">


        <form className="form-create-account" onSubmit={handleSubmit}>
            <label id="title" for="title">Log In</label><br /><br /><br />

            <label for="username">Username</label><br />
            <input type="text" placeholder="Username" name="username" onChange={e => setUserName(e.target.value)}/><br /><br />

           
            <label for="password">Password</label><br />
            <input type="password" placeholder="Password" name="password" onChange={e => setPassword(e.target.value)}/><br /><br />

            <label for="pword">Security Token<Link to="/recover-1"><AiOutlineQuestionCircle/></Link></label><br />

            <input type="text" placeholder="Token" name="idtoken" onChange={e => setIDToken(e.target.value)}/><br /><br />


            <button type="submit">Login</button><br /><br />

            <div className="form-footer" style={{textAlign: 'center'}}>
            <span className="login-in-tiny"><Link to="/forgot" style={{cursor: 'pointer',color: 'rgba(63, 227, 200, 1)', fontWeight: 'bold'}}><HoverText>Forgot Username / Password?</HoverText></Link></span><br /><br />

        <span className="login-in-tiny">Not a current Kassandra member? <Link to="/register" style={{cursor: 'pointer',color: 'rgba(63, 227, 200, 1)', fontWeight: 'bold'}}><HoverText>Create Account</HoverText></Link></span>
        </div>


        </form>

        </div>


        </>
    )
}
Login.propTypes = {
    setToken: PropTypes.func.isRequired

  };

import './form.css'
import 'react-phone-number-input/style.css'
import PhoneInput from 'react-phone-number-input'
import React from 'react';
import { useState } from "react";
import axios from "axios";
import { Link } from 'react-router-dom';

import styled from 'styled-components';

const HoverText = styled.p`
margin: 0;
padding: 0;
display: inline-block;
	:hover {
        color: black;
		cursor: pointer;
	}
`


function Create() {
    // const [value, setValue] = useState()
    const [username, setUsername] = useState()
    const [email, setEmail] = useState()
    const [password, setPassword] = useState()
    const [passconfirm, setPassconfirm] = useState()
    const [phone, setPhone] = useState()
    const [data, setData] = useState(null);

    const handleSubmit = () => {

      const data = {
        username: username,
        email: email,
        password: password,
        passconfirm: passconfirm,
        phone: phone
      }
      
      const headers = { 
          'content-type': 'application/json'
      }

      axios.post('http://45.77.112.99:8000/user', data, { headers }).then(res => {
        setData(res.data);
        setUsername('');
        setEmail('');
        setPassword('');
        setPassconfirm('');
        setPhone('');

        // setLoading(false);
      }).catch(err => {
        // setLoading(false);
        // setIsError(true);
      });
    }
   

    return (
        <>

        {/* <h1 className="form-title">Create Account</h1> */}

        <div className="account-form-div">


        <form className="form-create-account" onSubmit={handleSubmit}>
            <label id="title" for="title">Create Account</label><br /><br /><br />

            <label for="username">Username</label><br />
            <input
             type="text" 
             placeholder="Username" 
             name="username" 
             value={username}
             onChange={e => setUsername(e.target.value)} /><br /><br />


            <label for="email">Email</label><br />
            <input 
            type="text" 
            placeholder="Email" 
            name="email" 
            value={email}
            onChange={e => setEmail(e.target.value)} /><br /><br />

            <label for="pword">Password</label><br />
            <input 
            type="password" 
            placeholder="Password" 
            name="password" 
            value={password}
            onChange={e => setPassword(e.target.value)} /><br /><br />

            <label for="pwordconfirm">Password Confirmation</label><br />
            <input 
            type="password" 
            placeholder="Password Confirmation" 
            name="pwordconfirm" 
            value={passconfirm}
            onChange={e => setPassconfirm(e.target.value)} /><br /><br />

            <label for="phone">Phone</label><br />
            <PhoneInput className="phin"

                placeholder="Enter phone number"
                value={phone}
                onChange={setPhone}/><br /><br />

            <input id="checkbox" type="checkbox" placeholder="f" name="terms" />
            <span>I agree to the terms and conditions</span><br /><br /><br />

            <button
             type="submit"
             className="btn btn-primary mt-3"
            //  onClick={handleSubmit}
            >Create Account</button><br /><br />
            
            <div className="form-footer">

            <span className="login-in-tiny">Already a member? <Link to="/login" style={{cursor: 'pointer',color: 'rgba(63, 227, 200, 1)', fontWeight: 'bold'}}><HoverText>Log In</HoverText></Link></span>


            </div>

        </form>

        </div>


        </>
    )
}
export default Create
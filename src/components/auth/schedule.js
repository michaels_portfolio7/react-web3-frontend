import { ScheduleMeeting } from 'react-schedule-meeting';
import '../auth/schedule.css'

export default function Scheduler (){
    const availableTimeslots = [0, 1, 2, 3, 4, 5].map((id) => {
        return {
          id,
          startTime: new Date(new Date(new Date().setDate(new Date().getDate() + id)).setHours(9, 0, 0, 0)),
          endTime: new Date(new Date(new Date().setDate(new Date().getDate() + id)).setHours(17, 0, 0, 0)),
        };
      });

    return(
        
        <div className="scheduler" style={{width: '800px'}}>
                              <label id="title" for="title">Unable to Recover Security Token</label><br />
                  <h4>Please enter your Document Identification and your Social Security Number to recover your Security Token. </h4>
                  
                                  <label for="username">Available date & Time </label><br />
                      <input type="text" placeholder="07/18/2021  2:00 AM" name="username"/><br /><br />
              <ScheduleMeeting
                borderRadius={10}
                primaryColor="#3f5b85"
                eventDurationInMinutes={30}
                availableTimeslots={availableTimeslots}
                onStartTimeSelect={console.log}
            />

<button
                      type="submit"
                      className="btn btn-primary mt-3">Submit</button><br /><br />

        </div>

    )
}
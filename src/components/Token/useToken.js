import { useState } from 'react';



////////////////
// SESSION STOAGE
////////////////
// export default function useToken() {
//   const getToken = () => {
//     const tokenString = sessionStorage.getItem('token');
//     const userToken = JSON.parse(tokenString);
//     console.log(userToken)
//     return userToken?.access_token
//   };

//   const [token, setToken] = useState(getToken());

//   const saveToken = userToken => {
//     sessionStorage.setItem('token', JSON.stringify(userToken));
//     setToken(userToken.access_token);
//   };

//   return {
//     setToken: saveToken,
//     token
//   }
// }




////////////////
// LOCALSTORAGE
////////////////


export default function useToken() {
    const getToken = () => {
      const tokenString = localStorage.getItem('token');
      const userToken = JSON.parse(tokenString);
      console.log(userToken)
      return userToken?.acess_token
    };
  
    const [token, setToken] = useState(getToken());
  
    const saveToken = userToken => {
      localStorage.setItem('token', JSON.stringify(userToken));
      setToken(userToken.acess_token);
    };
  
    return {
      setToken: saveToken,
      token
    }
  }
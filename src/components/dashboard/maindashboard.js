import React from "react"
import '../dashboard/maindashboard.css'
import './cards.css'
import NavigationUser
 from "../navigation-user"


export default function MainDashboard () {


    return(
        <>
                    <NavigationUser />

            <div className="dashboard-container">


                    <div className="dashboard-grid">
                    <h1>Profile</h1>

                        <div className="dash-row-1">

                        {/* ACCOUNT */}
                            <div className="dash-box">
                                <div class="flip-card">

                                    <div class="flip-card-inner">

                                        <div class="flip-card-front">
                                            <h1>Account</h1>
                                        </div>

                                        <div class="flip-card-back">
                                            <p>User Information</p>
                                            <p>Bank Accounts</p>
                                            <p>Suspend</p>
                                            <p>Delete</p>

                                        </div>

                                    </div>
                                </div>
                        

                            </div>
                        {/* Platform */}

                        <div className="dash-box">
                                <div class="flip-card">

                                    <div class="flip-card-inner">

                                        <div class="flip-card-front">
                                            <h1>Platform</h1>
                                        </div>

                                        <div class="flip-card-back">
                                            <p>Feature Overview</p>
                                            <p>Startup Wizard</p>
                                            <p>Terms and Conditions</p>

                                        </div>

                                    </div>
                                </div>

                            </div>
                        {/* Performance */}

                            <div className="dash-box">
                                <div class="flip-card">

                                    <div class="flip-card-inner">

                                        <div class="flip-card-front">
                                            <h1>Performance</h1>
                                        </div>

                                        <div class="flip-card-back">
                                            <p>Update Trade</p>
                                            <p>Bank Investments</p>
                                            <p>Kassandra Merit</p>

                                        </div>

                                    </div>
                                </div>

                            </div>
            
                        </div>

                        <div className="dash-row-2">

                        {/* SECURITY */}

                            <div className="dash-box">
                                <div class="flip-card">

                                    <div class="flip-card-inner">

                                        <div class="flip-card-front">
                                            <h1>Security</h1>
                                        </div>

                                        <div class="flip-card-back">
                                            <p>Account Activity</p>
                                            <p>API Keys</p>
                                            <p>2FA</p>
                                            <p>Trusted Devices</p>

                                        </div>

                                    </div>
                                </div>
                            </div>

                            {/* ACCOMPLISHMENTS */}

                            <div className="dash-box">
                                <div class="flip-card">

                                    <div class="flip-card-inner">

                                        <div class="flip-card-front">
                                            <h1>Accomplishments</h1>
                                        </div>

                                        <div class="flip-card-back">
                                            <p>Available</p>
                                            <p>Complete</p>

                                        </div>

                                    </div>
                                </div>
                            </div>


                            { /* FAVORITES */}

                            <div className="dash-box">
                                <div class="flip-card">

                                    <div class="flip-card-inner">

                                        <div class="flip-card-front">
                                            <h1>Favorites</h1>
                                        </div>

                                        <div class="flip-card-back">
                                            <p>Coins</p>
                                            <p>Markets</p>
                                            <p>Members</p>
                                        </div>

                                    </div>
                                </div>


                            </div>
                        </div>

                        <div className="dash-row-3">

                        { /* PORTFOLIO */}

                            <div className="dash-box">

                                <div class="flip-card">

                                    <div class="flip-card-inner">

                                        <div class="flip-card-front">
                                            <h1>Portfolio</h1>
                                        </div>

                                        <div class="flip-card-back">
                                            <p>Balance</p>
                                            <p>Deposits</p>
                                            <p>Withdrawals</p>
                                        </div>

                                    </div>
                                    </div>


                            </div>

                            { /* PASSIVE INCOME */}

                            <div className="dash-box">
                                <div class="flip-card">
                                    <div class="flip-card-inner">

                                        <div class="flip-card-front">
                                            <h1>Passive Income</h1>
                                        </div>

                                        <div class="flip-card-back">
                                            <p>Loans</p>
                                            <p>Staking</p>
                                            <p>Referrals</p>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            { /* ORDERS */}

                            <div className="dash-box">
                                <div class="flip-card">

                                    <div class="flip-card-inner">

                                        <div class="flip-card-front">
                                            <h1>Orders</h1>
                                        </div>

                                        <div class="flip-card-back">
                                            <p>Open</p>
                                            <p>Closed</p>
                                            <p>Cancelled</p>
                                        </div>

                                    </div>
                                    </div>


                            </div>
                        </div>

                    </div>

            </div>
        </>
    )
}

import React, {useState} from "react";
import card from '/Users/michael/VSCodeProjects/cassandra/my-app/src/img/card02.png'
import './cards.css'

export default function TestBox () {
    const [style, setStyle] = useState({display: 'none'});

    return(
        <>
   

            <div class="flip-card">

                <div class="flip-card-inner">

                    <div class="flip-card-front">
                        <h1>Account</h1>
                    </div>

                    <div class="flip-card-back">
                        <p>User Information</p>
                        <p>Bank Accounts</p>
                        <p>Suspend</p>
                        <p>Delete</p>

                    </div>

                </div>
            </div>
        </>
    )
}
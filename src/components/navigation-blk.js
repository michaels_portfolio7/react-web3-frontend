import React, {useState} from 'react';
import './navigation-blk.css';

import { AiOutlineMenu } from 'react-icons/ai';


import Sidenav from "./sidenav"
import { Link } from 'react-router-dom';


import Logo from './library/logo';



import styled from 'styled-components';

const HoverText = styled.p`
	:hover {
        color: rgb(63, 227, 200);
		cursor: pointer;
	}
`



function NavigationBLK() {

    const [show, setShow]=useState(false)

        return(
            <div className="navigationblk">
                <div class="logo-kass">
                <Link to="/"><Logo/></Link>
                </div>
                <div className="nav-leftblk">
                    <ul className="nav-pagesblk">
                        <li id="logo-li"></li>
                        <li id="nav-li"><Link to="/learningplatform" style={{color: 'white',textDecoration: 'none'}}><HoverText>Learning Platform</HoverText></Link></li>
                        <li id="nav-li"><Link to="/tradingplatform" style={{color: 'white',textDecoration: 'none'}}><HoverText>Trading Platform</HoverText></Link></li>
                        <li id="nav-li"><Link to="/whykassandra" style={{color: 'white',textDecoration: 'none'}}><HoverText>Why Kassandra?</HoverText></Link></li>
                    </ul>
                </div>

                <div className="nav-rightblk">
                    <ul className="nav-account">
                        <li><Link to="/login" style={{color: 'white',textDecoration: 'none'}}><HoverText>Log In</HoverText></Link></li>
                        <li><Link to="/register"style={{color: 'white',textDecoration: 'none'}}><HoverText>Create Account</HoverText></Link></li>
                    </ul>
                </div>

                <div className="hamburger-btn">
                    <span class="menu-hamburger" onClick={()=>setShow(true)}>
                    <AiOutlineMenu />
                    </span>
                </div>
                {/* <div className="floatx">                                                
                     <span class="menu-close" onClick={()=>setShow(true)}>
                        <AiOutlineClose />
                    </span>
                </div> */}
                

                {     
                show?<Sidenav /> :null
                }

            </div>

        )
}

export default NavigationBLK
import React from 'react';
import ReactDOM from 'react-dom';
import Modal from 'react-modal';
import '../web3/wallet.css'
import { AiFillCloseCircle } from 'react-icons/ai';
import MetamaskIcon from '../web3/icons/metamask.jpeg'
import WalletConnect from '../web3/icons/walletconnect.jpeg'

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
  },
};

// Make sure to bind modal to your appElement (https://reactcommunity.org/react-modal/accessibility/)
Modal.setAppElement('#root');

export default function IsModal() {
  let subtitle;
  const [modalIsOpen, setIsOpen] = React.useState(false);

  function openModal() {
    setIsOpen(true);
  }

  function afterOpenModal() {
    // references are now sync'd and can be accessed.
    // subtitle.style.color = '#f00';
  }

  function closeModal() {
    setIsOpen(false);
  }

  return (
    <div>
        			<p>1. Choose Wallet: <span onClick={openModal}>Click Here</span> </p>

      {/* <button onClick={openModal}>Open Modal</button> */}
      <Modal
        isOpen={modalIsOpen}
        onAfterOpen={afterOpenModal}
        onRequestClose={closeModal}
        style={customStyles}
        contentLabel="Example Modal"
      >
                  <AiFillCloseCircle onClick={closeModal} />
                  <br/>

        <div>Connect to a wallet</div>
<br/>
        <div className="connectbutton">
                <span onClick={closeModal}><img width="15px"  src={MetamaskIcon}/> MetaMask </span>
            </div>
            <br/>

            <div className="connectbutton">
                <span onClick={closeModal}><img width="15px"  src={WalletConnect}/> WalletConnect </span>
            </div>

      </Modal>
    </div>
  );
}
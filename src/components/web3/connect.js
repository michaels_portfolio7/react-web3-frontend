
import React, {useState, useEffect} from 'react'
import {ethers} from 'ethers'

const WalletCardEthers = () => {

	const [errorMessage, setErrorMessage] = useState(null);
	const [defaultAccount, setDefaultAccount] = useState(null);
	const [userBalance, setUserBalance] = useState(null);
	const [connButtonText, setConnButtonText] = useState('Sync Wallet');
	const [provider, setProvider] = useState(null);

	const connectWalletHandler = () => {
		if (window.ethereum && defaultAccount == null) {
			// set ethers provider
			setProvider(new ethers.providers.Web3Provider(window.ethereum));

			// connect to metamask
			window.ethereum.request({ method: 'eth_requestAccounts'})
			.then(result => {
				setConnButtonText('Wallet Connected');
				setDefaultAccount(result[0]);
			})
			.catch(error => {
				setErrorMessage(error.message);
			});

		} else if (!window.ethereum){
			console.log('Need to install MetaMask');
			setErrorMessage('Please install MetaMask browser extension to interact');
		}
	}

useEffect(() => {
	if(defaultAccount){
	provider.getBalance(defaultAccount)
	.then(balanceResult => {
		setUserBalance(ethers.utils.formatEther(balanceResult));
	})
	};
}, [defaultAccount]);
	
	return (
		<div className='walletCard'>
			<br/>
			<p>2. Sync Wallet: <span onClick={connectWalletHandler}>Click Here</span> </p>
			{/* <button style={{width: '200px'}} onClick={connectWalletHandler}>{connButtonText}</button> */}
			<div className='accountDisplay'>
				<h4>Address: {defaultAccount}</h4>
			</div>
			<div className='balanceDisplay'>
				<h4>Balance: {userBalance}</h4>
			</div>
			{errorMessage}
		</div>
	);
}

export default WalletCardEthers;

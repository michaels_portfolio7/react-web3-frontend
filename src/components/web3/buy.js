import { useState } from "react";
import { ethers } from "ethers";
// import ErrorMessage from "./ErrorMessage";
// import TxList from "./TxList";
import TxList from '../web3/TxList'
import ErrorMessage from "./ErrorMessage";
const startPayment = async ({ setError, setTxs, ether, addr }) => {
  try {
    if (!window.ethereum)
      throw new Error("No crypto wallet found. Please install it.");

    await window.ethereum.send("eth_requestAccounts");
    const provider = new ethers.providers.Web3Provider(window.ethereum);
    const signer = provider.getSigner();
    ethers.utils.getAddress(addr);
    const tx = await signer.sendTransaction({
      to: addr,
      value: ethers.utils.parseEther(ether)
    });
    console.log({ ether, addr });
    console.log("tx", tx);
    setTxs([tx]);
  } catch (err) {
    setError(err.message);
  }
};

export default function Buy() {
  const [error, setError] = useState();
  const [txs, setTxs] = useState([]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    const data = new FormData(e.target);
    setError();
    await startPayment({
      setError,
      setTxs,
      ether: data.get("ether"),
      addr: data.get("addr")
    });
  };

  return (
    <form className="m-4" onSubmit={handleSubmit}>
      <div style={{width: '500px'}}> 
      <br/>
			<p>3. Buy / Exchange $Token </p>
              <input
                type="text"
                name="addr"
                className=""
                placeholder="Would be hidden and coded in"
              />
          
              <input
                name="ether"
                type="text"
                className=""
                placeholder="Amount in ETH"
              />


        <footer className="p-4">
          <button
            type="submit"
            className=""
            style={{width: '200px'}}
          >
            BUY $TOKEN
          </button>
          {/* {error}
          {txs} */}
          <ErrorMessage message={error} />
          <TxList txs={txs} />
        </footer>
      </div>
    </form>
  );
}

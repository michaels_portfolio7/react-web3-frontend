import React from 'react';
import './header.css'


import SocialComponent from './socialcomponent';
import ImageTypemark from './library/typemark';
import BackgroundGlobe from './library/backgroundglobe';
import { Link } from 'react-router-dom';

function Header () {

    return (
        <div className="main">
            <div className="heading">
                <div className="heading-center">
                    <ImageTypemark />
                    <div className="btn-join">
                        <Link to="/register"> <button className="btn-join-now" style={{color: 'white'}}>Hello Now</button></Link>
                    </div>

                </div>


            </div>

            <SocialComponent />

            <div className="world">
                <BackgroundGlobe />
            </div>
        </div>
    )

}

export default Header
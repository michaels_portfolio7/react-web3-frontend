import '../components/footer.css'

function Footer () {

    return (
        <div className="footer">
            <p>© Kassandra 2021</p>
        </div>
    )

}

export default Footer
import userlogout from '../../img/user-logout.png'

export default function UserLogout(){
    return(
        <>
    <img className="logo" alt="logout" src={userlogout} style={{opacity: '0.4'}}/>
       </>
        )
}
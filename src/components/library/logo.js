import kassandralogo from '../../img/logo.png'

export default function Logo(){
    return(
        <img className="logo" alt="logo" src={kassandralogo} />
    )
}



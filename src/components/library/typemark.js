import typemark from '../../img/typemark-logo.png'

export default function ImageTypemark(){
    return(
        <>
        <img width="100%" alt="typemark" src={typemark} />
        </>
        )
}
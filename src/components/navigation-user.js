import React, {useState} from 'react';
import './navigation-blk.css';

import { AiOutlineMenu } from 'react-icons/ai';

// import usericon from '/Users/michael/VSCodeProjects/cassandra/my-app/src/img/user-login.png'
// import userlogout from '/Users/michael/VSCodeProjects/cassandra/my-app/src/img/user-logout.png'

import UserIcon from './library/usericon';
import UserLogout from './library/userlogout';
import Logo from './library/logo';

import Sidenav from "./sidenav"
import { Link } from 'react-router-dom';

function NavigationUser() {

    const [show, setShow]=useState(false)

        return(
            <div className="navigationblk" style={{backgroundColor: 'rgb(22, 22, 22)'}}>
                <div class="logo-kass">
                <Link to="/"><Logo/></Link>
                </div>
                <div className="nav-leftblk" style={{backgroundColor: 'rgb(22, 22, 22)'}}>
                    <ul className="nav-pagesblk">
                        <li id="logo-li"></li>
                        <li id="nav-li"><Link to="/learningplatform" style={{color: 'white',textDecoration: 'none'}}>Learning Platform</Link></li>
                        <li id="nav-li"><Link to="/tradingplatform" style={{color: 'white',textDecoration: 'none'}}>Trading Platform</Link></li>
                        <li id="nav-li"><Link to="/whykassandra" style={{color: 'white',textDecoration: 'none'}}>Why Kassandra?</Link></li>
                    </ul>
                </div>

                <div className="nav-rightblk" style={{backgroundColor: 'rgb(22, 22, 22)'}}>
                    <ul className="nav-account">
                        <li><Link to="/login" style={{color: 'white',textDecoration: 'none'}}><UserIcon/></Link></li>
                        <li><Link to="/register"style={{color: 'white',textDecoration: 'none'}}><UserLogout/></Link></li>
                    </ul>
                </div>

                <div className="hamburger-btn">
                    <span class="menu-hamburger" onClick={()=>setShow(true)}>
                    <AiOutlineMenu />
                    </span>
                </div>
                {/* <div className="floatx">                                                
                     <span class="menu-close" onClick={()=>setShow(true)}>
                        <AiOutlineClose />
                    </span>
                </div> */}
                

                {     
                show?<Sidenav /> :null
                }

            </div>

        )
}

export default NavigationUser
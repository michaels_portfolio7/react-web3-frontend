import React from 'react';
import Collapsible from 'react-collapsible';
import '../termsconditions/style.css'
import { BsChevronDown } from "react-icons/bs"; 

export default function TermsConditions () {

    return (
        <div className="terms-container">
          <h1>Terms and Conditions</h1>

          <div className='eligibility'>
              <Collapsible trigger={["Eligibility", <BsChevronDown  />]} triggerStyle={{ fontSize: '20px'}}>
        <p>Test</p>
              </Collapsible><br/>
          </div>


          <div className='account'>

             <Collapsible trigger={["Account", <BsChevronDown style={{paddingLeft: '7px'}}/>]} triggerTagName="test" triggerStyle={{ fontSize: '20px'}}>
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
                Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32.
              </p>
                  <div className="test">

                  <Collapsible trigger={["Lorem Ipsum", <BsChevronDown />]} style={{marginLeft: '40px', fontSize: '14px'}}>
                      <p>Test</p>
                    </Collapsible>

                    <Collapsible trigger={["Lorem Ipsum", <BsChevronDown />]} style={{paddingLeft: '40px', fontSize: '14px'}}>
                      <p>Test</p>
                    </Collapsible>

                    <Collapsible trigger={["Lorem Ipsum", <BsChevronDown />]} style={{paddingLeft: '40px', fontSize: '14px'}}>
                      <p>Test</p>
                    </Collapsible>

                    <Collapsible trigger={["Lorem Ipsum", <BsChevronDown />]} style={{paddingLeft: '40px', fontSize: '14px'}}>
                      <p>Test</p>
                    </Collapsible>




                  </div>

                <p>
                  It can even be another Collapsible component. Check out the next
                  section!
                </p>
             </Collapsible>
      
           </div>



          <div className='risk'>
          <Collapsible trigger={["Risk", <BsChevronDown  style={{paddingLeft: '45px'}} />]} triggerStyle={{ fontSize: '20px'}}>
        <p>Test</p>
              </Collapsible><br/>
          </div>



 
      </div>
    )

}
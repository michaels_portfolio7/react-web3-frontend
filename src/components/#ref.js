import React, { useState } from "react";

const App = () => {
    const [style, setStyle] = useState("cont");
    
    const changeStyle = () => {
    
      setStyle("cont2");
    };
  
    return (
      <>
        <div className="App">CHANGE CSS STYLING WITH ONCLICK EVENT</div>
        <div className={style}>
          <button className="button" onClick={changeStyle}>
            Click me!
          </button>
        </div>
      </>
    );
  };
  export default App;
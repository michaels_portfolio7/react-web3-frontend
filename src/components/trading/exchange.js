import '../trading/trading.css'


export default function Exchange(){
    return(
        <div> 
            <div className="Open">
                <h4>Available</h4>
                <ul>
                <li>10.000012 <span>BTC</span></li>
                <li>10.000012 <span>ETH</span></li>
                </ul>

            </div>

            <div className="buy-sell-buttons">
                <button>Buy BTC</button>
                <button>Sell BTC</button>            
            </div>

            <div className="text-input-component">
                <h4>Price</h4>
                <ul>
                    <li>Buy</li>
                    <li>Sell</li>
                    <li>Last Price</li>
                </ul>   
                <input type={Number} placeholder="100,000,000.00069352 btc"/>
            </div>

            <div className="">
                <h4>Quantity</h4>
                <ul>
                    <li>0%</li>
                    <li>25%</li>
                    <li>50%</li>
                    <li>75%</li>
                    <li>100%</li>
                </ul>   
                <input type={Number} placeholder="100,000,000.00069352 btc"/>
            </div>
        </div>
    )
}
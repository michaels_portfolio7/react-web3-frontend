
import { FaBitcoin } from 'react-icons/fa';
import { FaEthereum } from 'react-icons/fa';
import '../trading/trading.css'

export default function CurrencyPairs(){
    return(
        <div>
            <div>
                <span><FaEthereum/>ETH</span><span><FaBitcoin/>BTC</span>

            </div>
            <div className="Open">
                <h4>Price</h4>
                <ul>
                <li>100,000 <span>BTC</span></li>
                <li>$100,000.00 <span>USD</span></li>
                </ul>

            </div>
        </div>
    )
}
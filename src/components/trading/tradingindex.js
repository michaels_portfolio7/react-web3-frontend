import '../trading/trading.css'
import CurrencyPairs from './currencypairs'
import Exchange from './exchange'
import OpenBuySell from './openbuysell'
import OrderBook from './orderbook'

export default function TradingDashboard(){
    return(
        <div className="trading-container">


                <div className="currency-pair">
                    <h4>Currency Pair</h4>
                    <CurrencyPairs/>
                </div>

                <div className="volume-spread">
                <OpenBuySell />
                <OpenBuySell />

                </div>

                <div className="buy-sell">
                    <Exchange />

                </div>


                <div className="chart">
                    <h4>Chart</h4>
                </div>
                

                <div className="order-book">
                    <h4>Order Book</h4>
                    <OrderBook/>
                </div>
                
        
        </div>
    )
}
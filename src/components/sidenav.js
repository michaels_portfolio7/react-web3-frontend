// import React, {useState} from 'react';

import './sidenav.css';
import { FaFacebookF } from 'react-icons/fa';
import { FaLinkedinIn } from 'react-icons/fa';
import { FaTwitter } from 'react-icons/fa';
import { FaRedditAlien } from 'react-icons/fa';
import { AiOutlineClose } from 'react-icons/ai';

function Sidenav() {
    // const [show, setShow]=useState(false)

    return (
        <div className="sidenav">
            <span className="menu-close" >
                <AiOutlineClose />
            </span>
            <ul className="nav-pages-sidebar">
                <li id="nav-li">Learning Platform</li>
                <li id="nav-li">Trading Platform</li>
                <li id="nav-li">Why Kassandra?</li>
            </ul>    
            <ul className="nav-account-sidebar">
                <li>Log In</li>
                <li>Create Account</li>
            </ul>
            <div className="sidebar-bottom">
            <ul className="social-components-sidebar"> 
                <li><FaFacebookF/></li>
                <li><FaLinkedinIn/></li>
                <li><FaTwitter/></li>
                <li><FaRedditAlien/></li>
            </ul>

            </div>
            

        </div>
    )

}
export default Sidenav
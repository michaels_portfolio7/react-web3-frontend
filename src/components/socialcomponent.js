import React from 'react';
import './header.css'

import facebook from '../img/icon-facebook.png'
import linkedin from '../img/icon-linkedin.png'
import twitter from '../img/icon-twitter.png'
import reddit from '../img/icon-reddit.png'

export default function SocialComponent(){
    return(
        <div className="social-components">
        <ul>
            <li><img src={facebook} alt="fb"/></li>
            <li><img src={linkedin}alt="linkedin"/></li>
            <li><img src={twitter}alt="twitter"/></li>
            <li><img src={reddit}alt="reddit"/></li>

        </ul>

    </div>
    )
}

